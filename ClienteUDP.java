import java.io.*;
import java.net.*;

public class UDPCliente {
	
	public static void main(String args[]) throws Exception
	{
	try {
	String servidorHostName = new String ("127.0.0.1"); 
	if (args.length > 0)
	servidorHostName = args[0];

		
		DatagramSocket clienteSocket = new DatagramSocket();
		BufferedReader user_buff = new BufferedReader(new InputStreamReader(System.in)); 
		InetAddress IPAddress = InetAddress.getByName(servidorHostName);  

		byte[] envioDado = new byte[1024]; 
		byte[] recebimentoDado = new byte[1024]; 
		  
		System.out.print("Escreva uma mensagem: ");  
		String msg = user_buff.readLine(); 
		envioDado = msg.getBytes();    
		
	     System.out.println ("Enviando mensagem de " + envioDado.length + " bytes para o servidor");
		 DatagramPacket envioPacote = new DatagramPacket(envioDado, envioDado.length, IPAddress, 8000); 
		 clienteSocket.send(envioPacote); 		  
		 clienteSocket.close(); 
	 	    }
	    catch (UnknownHostException ex) { 
		  System.err.println(ex);
	    }
	    catch (IOException ex) {
				 System.err.println(ex);
		    }
	}
}