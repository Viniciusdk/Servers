import java.io.*;
import java.net.Socket;

import javax.swing.JOptionPane;

public class ClienteTCP {
	
	public static void main(String argv[]) throws Exception
	{
			String ip = "localhost";
			int porta;
			String frase="a";
			String str;
			
			str=JOptionPane.showInputDialog("Digite a Porta","8000");
			porta=Integer.parseInt(str);
			while(frase!=null)
			{
				BufferedReader doUsuario= new BufferedReader(new InputStreamReader(System.in));
				Socket socketCliente=new Socket(ip, porta);
				DataOutputStream paraServidor= new DataOutputStream(socketCliente.getOutputStream());
				BufferedReader doServidor= new BufferedReader( new InputStreamReader(socketCliente.getInputStream()));
				frase= JOptionPane.showInputDialog("Digite a Mensagem");
				paraServidor.writeBytes(frase + "\n");
				socketCliente.close();
			}
	}
}