import java.io.*;
import java.net.*;


public class UDPServer {
	
	public static void main(String args[]) throws Exception
	{
	try
	{
	DatagramSocket servidorSocket = new DatagramSocket(8000);

	  byte[] dadoRecebido = new byte[1024]; 	
	  byte[] dadoEnviado  = new byte[1024]; 
	  while(true) 
	    { 
	      dadoRecebido = new byte[1024]; 

	      DatagramPacket recebimentoPacote = new DatagramPacket(dadoRecebido, dadoRecebido.length); 

	      servidorSocket.receive(recebimentoPacote); 

	      String msgRecebida = new String(recebimentoPacote.getData());

	      InetAddress IPAddress = recebimentoPacote.getAddress(); 
	      int porta = recebimentoPacote.getPort();

	      System.out.println ("Servidor:" + msgRecebida);
	     }
	 }
	  catch (SocketException ex) {
	    System.out.println("UDP porta 8000 estÃ¡ ocupado.");
	    System.exit(1);
	  }
}
}