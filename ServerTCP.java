import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

public class ServerTCP{

	public static void main(String args[]) throws Exception
	{
		String fraseCliente;
		String fraseMaiuscula;
		String str;
		int porta;
		str=JOptionPane.showInputDialog("Digite A Porta", "8000");
		porta=Integer.parseInt(str);
		ServerSocket socketRecepcao= new ServerSocket(porta);
		while(true)
		{
			Socket socketConexao=socketRecepcao.accept();
			BufferedReader doCliente= new BufferedReader(new InputStreamReader(socketConexao.getInputStream()));
			DataOutputStream paraCliente= new DataOutputStream(socketConexao.getOutputStream());
			fraseCliente= doCliente.readLine();
			JOptionPane.showMessageDialog(null, fraseCliente,"Mensagem Recebida", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}